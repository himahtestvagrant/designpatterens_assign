import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Total extends Pageinitialiser{

    public Total(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//*[@data-test='cjs-price']")
    private WebElement flightCost;

    public String ticketCost(){
        return flightCost.getText();
    }
}
