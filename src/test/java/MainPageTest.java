import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class MainPageTest {
    public static WebDriver driver;
    public String demoUrl = "https://www.thetrainline.com/" ;
    @BeforeMethod
    public void setupDriver(){

        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.navigate().to(demoUrl);
    }
    @Test
    public void Travel() {

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        Details d= new Details(driver);
        d.acceptCooki();
        d.location();
        d.destination();
        d.returnBack();
        d.startDate();
        d.endDate();
        d.clickSubmit();
        Total t = new Total(driver);
        System.out.println(t.ticketCost());

        Assert.assertEquals(t.ticketCost(), "$151.64");







    }

   /* @AfterMethod
    public void quitBrowser(){
        driver.quit();
    }*/

}