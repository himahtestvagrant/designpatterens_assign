import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Details extends  Pageinitialiser{
    private static WebDriver driver;
    public Details(WebDriver driver){
        super(driver);
        this.driver= driver;

    }
    @FindBy(id="onetrust-accept-btn-handler")
    private WebElement cookiesAccept;
    @FindBy(id="from.search")
    WebElement from_search;
    @FindBy(id="to.search")
    WebElement to_search;
    @FindBy(id="return")
    WebElement rd;
    @FindBy(id="page.journeySearchForm.outbound.title")
    WebElement date;
    @FindBy(id="page.journeySearchForm.inbound.title")
    WebElement nextDate;
    @FindBy(css="button[data-test='submit-journey-search-button']\n")
    WebElement submit;

    public void acceptCooki(){
        cookiesAccept.click();
    }
    public  void location(){
        from_search.sendKeys("Sheffield");
    }
    public void destination(){
        to_search.sendKeys("London");
    }
    public void returnBack(){
        rd.click();
    }
    public void startDate(){
        date.sendKeys("23-Jul-21");
    }
    public void endDate(){
        nextDate.sendKeys("12-Aug-21");
    }
    public void clickSubmit(){
       ((JavascriptExecutor)driver).executeScript("arguments[0].click();", submit);
    }


}
